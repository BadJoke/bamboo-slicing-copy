using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MouseSlice : MonoBehaviour
{

    [SerializeField]
    private ScoreBar _scoreBar;

    private int _slicingType;

    // How far away from the slice do we separate resulting objects
    public float separation;

    // Do we draw a plane object associated with the slice
    private Plane slicePlane = new Plane();

    // Reference to the line renderer
    public ScreenLineRenderer lineRenderer;

    private MeshCutter meshCutter;
    private TempMesh biggerMesh, smallerMesh;

    // Use this for initialization
    void Start()
    {
        // Initialize a somewhat big array so that it doesn't resize
        meshCutter = new MeshCutter(256);
    }

    private void OnEnable()
    {
        lineRenderer.OnLineDrawn += OnLineDrawn;
    }

    private void OnDisable()
    {
        lineRenderer.OnLineDrawn -= OnLineDrawn;
    }

    private void OnLineDrawn(Vector3 start, Vector3 end, Vector3 depth, int objectType)
    {
        _slicingType = objectType;

        Vector3 planeTangent;

        if (start.x > end.x)
        {
            planeTangent = (end - start).normalized;
        }
        else
        {
            planeTangent = (start - end).normalized;
        }

        // if we didn't drag, we set tangent to be on x
        if (planeTangent == Vector3.zero)
            planeTangent = Vector3.right;

        var normalVec = Vector3.Cross(depth, planeTangent);
        normalVec.z = 0;

        SliceObjects(start, normalVec);
    }

    void SliceObjects(Vector3 point, Vector3 normal)
    {
        var toSlice = GameObject.FindGameObjectsWithTag("Sliceable").ToList();

        if (_slicingType == 8)
        {
            toSlice = toSlice.FindAll(s => s.layer == _slicingType || s.layer == 9);
        }
        else
        {
            toSlice = toSlice.FindAll(s => s.layer == _slicingType);
        }

        // Put results in positive and negative array so that we separate all meshes if there was a cut made
        List<Transform> lower = new List<Transform>();
        List<Transform> upper = new List<Transform>();

        bool slicedAny = false;
        foreach (var obj in toSlice)
        {
            // We multiply by the inverse transpose of the worldToLocal Matrix, a.k.a the transpose of the localToWorld Matrix
            // Since this is how normal are transformed
            var transformedNormal = ((Vector3)(obj.transform.localToWorldMatrix.transpose * normal)).normalized;

            //Convert plane in object's local frame
            slicePlane.SetNormalAndPosition(
                transformedNormal,
                obj.transform.InverseTransformPoint(point));

            slicedAny = SliceObject(ref slicePlane, obj, lower, upper) || slicedAny;
        }

        upper.Sort(delegate (Transform c1, Transform c2)
        {
            return c1.position.y.CompareTo(c2.position.y);
        });

        if (upper.Count > 1)
        {
            List<Transform> children = upper.FindAll(s => s.gameObject.layer == 8 && s.gameObject.layer != 11);

            if(children.Count != 0)
            {
                Transform parent = children[0];
                children.RemoveAt(0);
                float offset = 3.5f;

                if (children.Count > 0)
                {
                    for (int i = 0; i < children.Count; i++)
                    {
                        SetParent(children[i], parent, offset * (i + 1));
                    }

                    Transform armor = upper.Find(s => s.gameObject.layer == 9);

                    if (armor != null)
                    {
                        armor.parent = parent;
                        armor.tag = "Sliced";
                        Destroy(armor.GetComponent<Rigidbody>());
                        Destroy(armor.GetComponent<Movement>());
                        Destroy(armor.GetComponent<ArmorSwinging>());
                    }
                }

                PushSlicedPiece(parent.GetComponent<Rigidbody>(), lineRenderer.GetDirection());
            }
        }
        else if (upper.Count == 1)
        {
            if (upper[0].gameObject.layer != 11)
            {
                upper[0].gameObject.layer = 10;
                PushSlicedPiece(upper[0].GetComponent<Rigidbody>(), lineRenderer.GetDirection());
            }
        }

        if (slicedAny)
        {
            _scoreBar.WasSliced();
        }
    }

    private void SetParent(Transform child, Transform parent, float offset)
    {
        child.SetParent(parent);
        child.localPosition = new Vector3(0, offset, 0);
        child.localRotation = Quaternion.Euler(0, 0, 0);
        child.tag = "Sliced";
        child.gameObject.layer = 10;
        Destroy(child.GetComponent<Rigidbody>());
        Destroy(child.GetComponent<Movement>());
    }

    private void PushSlicedPiece(Rigidbody rigidbody, Vector3 forceVector)
    {
        rigidbody.useGravity = true;
        rigidbody.constraints = RigidbodyConstraints.None;

        rigidbody.AddForce(forceVector, ForceMode.Impulse);
    }

    bool SliceObject(ref Plane slicePlane, GameObject obj, List<Transform> lowerObjects, List<Transform> upperObjects)
    {
        var mesh = obj.GetComponent<MeshFilter>().mesh;

        if (!meshCutter.SliceMesh(mesh, ref slicePlane) || obj.layer == 9)
        {
            // Put object in the respective list
            if (slicePlane.GetDistanceToPoint(meshCutter.GetFirstVertex()) >= 0)
                lowerObjects.Add(obj.transform);
            else
                upperObjects.Add(obj.transform);

            return false;
        }

        // Silly condition that labels which mesh is bigger to keep the bigger mesh in the original gameobject
        biggerMesh = meshCutter.PositiveMesh;
        smallerMesh = meshCutter.NegativeMesh;

        // Create new Sliced object with the other mesh
        obj.transform.parent = null;
        GameObject newObject = Instantiate(obj);
        newObject.tag = "Sliced";
        Destroy(newObject.GetComponent<Movement>());

        var newObjMesh = newObject.GetComponent<MeshFilter>().mesh;

        // Put the bigger mesh in the original object
        ReplaceMesh(mesh, biggerMesh, obj.GetComponent<MeshCollider>()); ;
        ReplaceMesh(newObjMesh, smallerMesh, newObject.GetComponent<MeshCollider>());

        if (obj.layer == 11)
        {
            obj.tag = "Sliced";

            Destroy(obj.GetComponent<BonusSwingnig>());
            Destroy(newObject.GetComponent<BonusSwingnig>());

            PushSlicedPiece(obj.GetComponent<Rigidbody>(), slicePlane.normal);
            PushSlicedPiece(newObject.GetComponent<Rigidbody>(), slicePlane.flipped.normal * 1.5f);

            Transform container = obj.transform.GetChild(0);
            container.parent = null;
            container.gameObject.SetActive(true);
        }
        else
        {
            lowerObjects.Add(obj.transform);
            upperObjects.Add(newObject.transform);
        }

        return true;
    }


    /// <summary>
    /// Replace the mesh with tempMesh.
    /// </summary>
    void ReplaceMesh(Mesh mesh, TempMesh tempMesh, MeshCollider collider = null)
    {
        mesh.Clear();
        mesh.SetVertices(tempMesh.vertices);
        mesh.SetTriangles(tempMesh.triangles, 0);
        mesh.SetNormals(tempMesh.normals);
        mesh.SetUVs(0, tempMesh.uvs);

        mesh.RecalculateTangents();
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();

        if (collider != null && collider.enabled)
        {
            collider.sharedMesh = mesh;
            collider.convex = true;
        }
    }
}
