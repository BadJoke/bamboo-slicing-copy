﻿using Assets;
using UnityEngine;

public class ScreenLineRenderer : MonoBehaviour {

    // Line Drawn event handler
    public delegate void LineDrawnHandler(Vector3 begin, Vector3 end, Vector3 depth, int objectType);
    public event LineDrawnHandler OnLineDrawn;

    Vector3 start;
    Vector3 end;
    Camera cam;

    public Material lineMaterial;

    private float _sliceDistance;

    [SerializeField]
    private Blade _blade;
    [SerializeField]
    private PanelsController _controller;
    [SerializeField]
    private ScoreBar _scoreBar;
    [SerializeField]
    private bool _fullSlice;

    private int _targetLayer;

    // Use this for initialization
    void Start () {
        cam = Camera.main;
        Options.State = States.Wait;
        _sliceDistance = -cam.transform.position.z;
    }

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Ray raycast = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit raycastHit = new RaycastHit();
            Physics.Raycast(raycast, out raycastHit);
            Transform obstacle = raycastHit.transform;

            if (_blade.Effect == null)
            {
                _blade.StartEffect();
            }

            if (obstacle)
            {
                _targetLayer = obstacle.gameObject.layer;

                if (obstacle.gameObject.layer.Equals(9))
                {
                    if (Options.IsStarted)
                    {
                        if (obstacle.tag.Equals("Lose"))
                        {
                            if (obstacle.position.y > 0 && raycast.origin.y > obstacle.position.y)
                            {
                                _scoreBar.StopAllCoroutines();
                                _controller.Lose();
                            }
                        }
                        else
                        {
                            _scoreBar.StopAllCoroutines();
                            _controller.Lose();
                        }
                    }
                }
            }

            if (Options.State == States.Wait)
            {
                if (obstacle != null)
                {
                    if (obstacle.tag.Equals("Sliceable"))
                    {
                        Options.State = States.Wait;
                        _blade.StopEffect();
                    }
                }
                else
                {
                    Options.State = States.Targeting;
                }
            }

            if (Options.State == States.Targeting || Options.State == States.Drawing)
            {
                end = cam.ScreenToViewportPoint(Input.mousePosition);

                if (obstacle)
                {
                    if (obstacle.tag == "Sliceable")
                    {
                        if (Options.State == States.Targeting)
                        {
                            start = cam.ScreenToViewportPoint(Input.mousePosition);
                            Options.State = States.Drawing;
                        }
                    }
                }
                else
                {
                    if (Options.State == States.Drawing)
                    {
                        Slice(_targetLayer);
                    }
                    else
                    {
                        Options.State = States.Targeting;
                    }
                }

                _blade.UpdateSlice();
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (_fullSlice == false && Options.State == States.Drawing)
            {
                Slice(_targetLayer);
            }

            Options.State = States.Wait;
            _blade.StopEffect();
        }
    }

    private void Slice(int layer)
    {
        // Finished dragging. We draw the line segment
        end = cam.ScreenToViewportPoint(Input.mousePosition);

        var startRay = cam.ViewportPointToRay(start);
        var endRay = cam.ViewportPointToRay(end);

        // Raise OnLineDrawnEvent
        OnLineDrawn?.Invoke(
            startRay.GetPoint(_sliceDistance),
            endRay.GetPoint(_sliceDistance),
            endRay.direction.normalized,
            layer);

        Options.State = States.Wait;
        _blade.StopEffect();
    }

    public Vector3 GetDirection()
    {
        Vector3 direction = Vector3.forward;

        if (end.x < start.x && end.y > start.y)
        {
            direction = Vector3.right;
        }
        else if (end.x < start.x && end.y < start.y)
        {
            direction = Vector3.left;
        }
        else if (end.x > start.x && end.y < start.y)
        {
            direction = Vector3.right;
        }
        else //if (end.x > start.x && end.y > start.y)
        {
            direction = Vector3.left;
        }

        return (direction + Vector3.up) * Options.PushForce;
    }
}
