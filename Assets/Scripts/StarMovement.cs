﻿using UnityEngine;

public class StarMovement : MonoBehaviour
{
    [SerializeField]
    private Transform _target;

    private float _speed = 4;
    private Vector3 _direction;
    private Rigidbody _rigidbody;

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        _direction = (_target.position - transform.position).normalized;
        _rigidbody.velocity = _direction * _speed;
    }
}