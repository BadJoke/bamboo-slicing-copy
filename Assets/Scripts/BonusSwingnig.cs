﻿using Assets;
using UnityEngine;

public class BonusSwingnig : MonoBehaviour
{
    [SerializeField]
    private float _amplitude;
    [SerializeField]
    private float _startAngle;
    [SerializeField]
    private float _speed;

    private void Update()
    {
        _startAngle += Time.deltaTime * _speed;
        transform.localPosition = new Vector3(transform.localPosition.x, Mathf.Sin(Mathf.Deg2Rad * _startAngle), transform.localPosition.z);
    }
}
