﻿using Assets;
using UnityEngine;

public class ArmorSwinging : MonoBehaviour
{
    [SerializeField]
    private float _topBorder;
    [SerializeField]
    private float _bottomBorder;
    private float _speedUp;
    private float _speedDown;
    private bool _isMovingdUp = true;

    private void Update()
    {
        _speedUp = Options.ArmorSwingingSpeed - Options.Speed;
        _speedDown = Options.ArmorSwingingSpeed;

        if (_isMovingdUp)
        {
            transform.Translate(Vector3.up * _speedUp * Time.deltaTime);

            if (transform.localPosition.y >= _topBorder)
            {
                _isMovingdUp = false;
            }
        }
        else
        {
            transform.Translate(Vector3.down * _speedDown * Time.deltaTime);

            if (transform.localPosition.y <= _bottomBorder)
            {
                _isMovingdUp = true;
            }
        }
    }
}
