﻿using Assets;
using UnityEngine;

public class GemMovement : MonoBehaviour
{
    private float _speed = 4;
    private Vector3 _direction;
    private Rigidbody _rigidbody;

    private void Start()
    {
        _speed = Random.Range(_speed - 2, _speed) / 2;
        _direction = Options.GemTarget.position - transform.position;
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        _rigidbody.velocity = _direction * _speed;
    }
}