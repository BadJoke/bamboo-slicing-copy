﻿using UnityEngine;

public class Blade : MonoBehaviour
{
    [SerializeField]
    private GameObject _effectPrefab;

    public GameObject Effect;

    public void UpdateSlice()
    {
        Vector3 position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 8f);
        position = Camera.main.ScreenToWorldPoint(position);
        transform.position = position;
    }

    public void StartEffect()
    {
        Effect = Instantiate(_effectPrefab, transform);
    }

    public void StopEffect()
    {
        Destroy(Effect);
    }
}
