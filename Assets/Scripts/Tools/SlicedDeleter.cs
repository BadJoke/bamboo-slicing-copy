﻿using UnityEngine;

public class SlicedDeleter : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Sliced"))
        {
            Destroy(other.gameObject);
        }
    }
}
