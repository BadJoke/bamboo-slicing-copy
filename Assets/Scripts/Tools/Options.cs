﻿using UnityEngine;

namespace Assets
{
    public static class Options
    {
        public static int Level;
        public static int GemCount;
        public static float Speed;
        public static float ArmorSwingingSpeed;
        public static float PushForce;
        public static States State;
        public static bool IsStarted;
        public static float LevelProgress;
        public static Transform GemTarget;
    }

    public enum States { Wait, Targeting, Drawing, End}
    public enum LevelType { Clear, Armor, Bonuses, ActiveArmor, ActiveBonuses}
}
