﻿using UnityEngine;

public class LoseZone : MonoBehaviour
{
    [SerializeField]
    private PanelsController _controller;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Sliceable") && other.gameObject.layer != 11)
        {
            _controller.Lose();
        }
    }
}
