﻿using Assets;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField]
    private bool _isClockwise;

    private Vector3 _direction;

    private void Start()
    {
        _direction = _isClockwise ? Vector3.up : Vector3.down;
    }

    private void FixedUpdate()
    {
        Vector3 move = Vector3.up * Options.Speed * Time.deltaTime;
        Vector3 rotate = _direction;

        transform.Translate(move);
        transform.Rotate(rotate);
    }
}
