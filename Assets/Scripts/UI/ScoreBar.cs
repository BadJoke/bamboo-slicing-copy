﻿using Assets;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBar : MonoBehaviour
{
    [SerializeField]
    private PanelsController _controller;
    [SerializeField]
    private Animator _infoAnimator;
    [SerializeField]
    private Animator _gemAnimator;
    [SerializeField]
    private Text _scoreBar;
    [SerializeField]
    private Image _progress;
    [SerializeField]
    private int _addedPoints;
    [SerializeField]
    private float _pointsSpeed;
    [SerializeField]
    private float _fillingSpeed;
    [SerializeField]
    private float _speed;
    [SerializeField]
    private int _slicesToWin;

    private int _slicesWinCount;
    private int _slicesLeft = 0;
    private int _score = 0;

    private IEnumerator _filling;

    private void Start()
    {
        _slicesLeft = 0;
        _slicesWinCount = Random.Range(_slicesToWin - 5, _slicesToWin + 6);
        _scoreBar.text = _score.ToString();
        _progress.fillAmount = 0;
        _filling = ProgressFilling(_progress.fillAmount, Options.LevelProgress);

        Options.State = States.Wait;
    }

    public void WasSliced()
    {
        StartCoroutine(AddPoints());
        StopCoroutine(_filling);

        _controller.PromptOff(_speed);
        _infoAnimator.SetTrigger("Appearing");
        _gemAnimator.SetTrigger("Appearing");

        Options.LevelProgress = (float)++_slicesLeft / _slicesWinCount;
        _filling = ProgressFilling(_progress.fillAmount, Options.LevelProgress);
        StartCoroutine(_filling);
    }

    private IEnumerator AddPoints()
    {
        for (int i = 0; i < _addedPoints; i++)
        {
            _scoreBar.text = (++_score).ToString();
            yield return new WaitForSeconds(1 / _pointsSpeed);
        }
    }

    private IEnumerator ProgressFilling(float startValue, float finishValue)
    {
        float step = (finishValue - startValue) / _fillingSpeed;

        while (startValue < finishValue)
        {
            startValue += step;
            _progress.fillAmount = startValue;

            if (_progress.fillAmount >= 0.33f && _controller.StarCount < 1)
            {
                _controller.IncreaseStarCount();
            }

            if (_progress.fillAmount >= 0.66f && _controller.StarCount < 2)
            {
                _controller.IncreaseStarCount();
            }

            if (_progress.fillAmount == 1f)
            {
                if (_controller.StarCount < 3)
                {
                    _controller.IncreaseStarCount();
                }
                _controller.Win();
            }

            yield return null;
        }
    }
}
