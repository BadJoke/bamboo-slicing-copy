﻿using Assets;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class LevelCustomizer : MonoBehaviour
{
    [SerializeField]
    private int _levelNumber;
    [SerializeField]
    private int _styleNumber;
    [SerializeField]
    private Text _level;
    [SerializeField]
    private Text _gemCount;
    [SerializeField]
    private List<Style> _styles;
    [SerializeField]
    private SpriteRenderer _background;
    [SerializeField]
    private Image _progressBar;
    [SerializeField]
    private List<MeshRenderer> _cylinders;
    [SerializeField]
    private GameObject _armorPrefab;
    [SerializeField]
    private float _armorSwingingSpeed;
    [SerializeField]
    private List<Material> _armorMaterials;
    [SerializeField]
    private List<Armor> _armors;
    [SerializeField]
    private GameObject _bonusPrefabX2;
    [SerializeField]
    private GameObject _bonusPrefabX3;
    [SerializeField]
    private List<Bonus> _bonuses;
    [SerializeField]
    private List<Material> _bonusMaterials;
    [SerializeField]
    private Transform _gemTarget;
    [SerializeField]
    private float _pushForce;

    private LevelType _type;
    private Transform _armor;
    private GameObject _bonus;

    private void Start()
    {
        Options.IsStarted = false;
        Options.GemTarget = _gemTarget;
        Options.ArmorSwingingSpeed = _armorSwingingSpeed;
        Options.PushForce = _pushForce;

        if (_levelNumber < 1)
        {
            if (PlayerPrefs.HasKey("Level"))
            {
                Options.Level = PlayerPrefs.GetInt("Level");
            }
            else
            {
                Options.Level = 1;
            }
        }
        else
        {
            Options.Level = _levelNumber;
        }
        Random.InitState(Options.Level);

        if (Options.Level < 6)
        {
            _type = (LevelType)(Options.Level - 1);
        }
        else
        {
            _type = (LevelType)Random.Range(1, 5);
        }

        _level.text = "LEVEL " + Options.Level;
        _gemCount.text = Options.GemCount.ToString();
        _armor = _armorPrefab.transform.GetChild(0);

        SetStyle();
        SetBonusStyle();

        switch (_type)
        {
            case LevelType.Clear:
                break;
            case LevelType.Armor:
                PlaceArmor();
                break;
            default:
                PlaceArmor();
                PlaceBonuses();
                break;
        }
    }

    private void SetStyle()
    {
        if (_styleNumber < 0)
        {
            _styleNumber = Random.Range(0, _styles.Count);
        }

        Sprite sprite = _styles[_styleNumber].Sprite;
        Color color = _styles[_styleNumber].Color;
        Material material = _styles[_styleNumber].Material;

        _background.sprite = sprite;
        _progressBar.color = color;
        _armor.GetComponent<MeshRenderer>().material = _armorMaterials[_styleNumber];

        foreach (var cylinder in _cylinders)
        {
            cylinder.material = material;
        }
    }

    private void SetBonusStyle()
    {
        List<MeshRenderer> bonuses = _bonusPrefabX2.GetComponentsInChildren<MeshRenderer>().ToList();
        bonuses.AddRange(_bonusPrefabX3.GetComponentsInChildren<MeshRenderer>().ToList());

        for (int i = 0; i < bonuses.Count; i++)
        {
            bonuses[i].material = _bonusMaterials[_styleNumber];

            if (_type < LevelType.ActiveBonuses)
            {
                bonuses[i].GetComponent<BonusSwingnig>().enabled = false;
            }
            else
            {
                bonuses[i].GetComponent<BonusSwingnig>().enabled = true;
            }
        }
    }

    private void PlaceArmor()
    {
        float y;

        for (int i = 0; i < _armors.Count; i++)
        {
            y = Random.Range(_armors[i].StartPosition, _armors[i].EndPosition + 1);
            _armorPrefab.transform.position = new Vector3(0, y, 0);

            y = 1 + (0.25f * Random.Range(0, 3));
            _armor.localScale = new Vector3(1.5f, y, 1.5f);

            if (_type < LevelType.ActiveArmor)
            {
                _armor.GetComponent<ArmorSwinging>().enabled = false;
            }
            else
            {
                _armor.GetComponent<ArmorSwinging>().enabled = true;
            }

            Instantiate(_armorPrefab);
        }
    }

    private void PlaceBonuses()
    {
        float y;

        for (int i = 0; i < _bonuses.Count; i++)
        {
            _bonus = Random.Range(0, 2) == 0 ? _bonusPrefabX2 : _bonusPrefabX3;

            y = Random.Range(_bonuses[i].StartPosition, _bonuses[i].EndPosition + 1);
            _bonus.transform.position = new Vector3(0, y, 0);

            Instantiate(_bonus);
        }
    }
}

[Serializable]
public class Style
{
    public Sprite Sprite;
    public Color Color;
    public Material Material;
}

[Serializable]
public class Armor
{
    public float StartPosition;
    public float EndPosition;
}

[Serializable]
public class Bonus
{
    public float StartPosition;
    public float EndPosition;
}