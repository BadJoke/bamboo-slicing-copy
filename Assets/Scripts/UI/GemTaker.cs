﻿using Assets;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(Rigidbody))]
public class GemTaker : MonoBehaviour
{
    [SerializeField]
    private Text _gemCountText;

    private void Start()
    {
        if (PlayerPrefs.HasKey("GemCount"))
        {
            Options.GemCount = PlayerPrefs.GetInt("GemCount");
        }
        _gemCountText.text = Options.GemCount.ToString();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Gem"))
        {
            Options.GemCount++;
            PlayerPrefs.SetInt("GemCount", Options.GemCount);
            PlayerPrefs.Save();

            _gemCountText.text = Options.GemCount.ToString();

            Destroy(other.gameObject);
        }
    }
}
