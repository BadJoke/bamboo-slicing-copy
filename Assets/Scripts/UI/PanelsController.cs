﻿using Assets;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PanelsController : MonoBehaviour
{
    [SerializeField]
    private GameObject _losePanel;
    [SerializeField]
    private Text _loseMessage;
    [SerializeField]
    private GameObject _winPanel;
    [SerializeField]
    private Text _winMessage;
    [SerializeField]
    private GameObject _prompt;
    [SerializeField]
    private GameObject _slicer;
    [SerializeField]
    private Animator _winAnimator;
    [SerializeField]
    private StarMovement[] _starMovements;
    [SerializeField]
    private Transform _interface;
    [SerializeField]
    private Transform _stars;
    [SerializeField]
    private Rotator[] _starRotators;

    public int StarCount { get; private set; } = 0;

    public void OnClick()
    {
        Options.Speed = 0;
        SceneManager.LoadScene("Gameplay");
    }

    public void Win()
    {
        _slicer.SetActive(false);
        _winPanel.SetActive(true);
        _winMessage.text = "LEVEL " + Options.Level + " COMPLETED";
        _stars.parent = _interface;
        _winAnimator.SetTrigger("Win");

        for (int i = 0; i < StarCount; i++)
        {
            _starRotators[i].enabled = true;
        }

        PlayerPrefs.SetInt("Level", Options.Level + 1);
        PlayerPrefs.Save();
    }

    public void Lose()
    {
        if (StarCount > 0)
        {
            Win();
            return;
        }

        if (_slicer.activeSelf == false)
        {
            return;
        }

        _slicer.SetActive(false);
        _losePanel.SetActive(true);
        _loseMessage.text = string.Format("LEVEL FAILED");
    }

    public void PromptOff(float speed)
    {
        if (Options.IsStarted == false)
        {
            Options.Speed = speed;
            Options.IsStarted = true;
            _prompt.SetActive(false);
        }
    }

    public void IncreaseStarCount()
    {
        _starMovements[StarCount++].enabled = true;
    }
}
