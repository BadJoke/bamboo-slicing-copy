﻿using UnityEngine;

public class StarTaker : MonoBehaviour
{
    [SerializeField]
    private Material[] _materials;
    [SerializeField]
    private Collider _collider;

    private void OnTriggerEnter(Collider other)
    {
        Destroy(other.gameObject);
        GetComponent<MeshRenderer>().materials = _materials;
        _collider.enabled = false;
    }
}
