﻿using System.Collections.Generic;
using UnityEngine;

public class GemContainer : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> _gems;

    private void Start()
    {
        int count = Random.Range(_gems.Count - 3, _gems.Count);

        for (int i = 0; i <= count; i++)
        {
            _gems[i].SetActive(true);
        }
    }
}
