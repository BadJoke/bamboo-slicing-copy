﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;

public class GAManager : MonoBehaviour
{
    private static GAManager playerInstance;

    void Awake(){
        DontDestroyOnLoad (this);
            
        if (playerInstance == null)
        {
            playerInstance = this;
        }
        else 
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        GameAnalytics.Initialize();
    }
}
